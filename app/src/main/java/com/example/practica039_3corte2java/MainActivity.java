package com.example.practica039_3corte2java;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    ListView lv;
    AdapterListView adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<alumnoItem> list = new ArrayList<>();
        list.add(new alumnoItem(getString(R.string.nombre1), getString(R.string.matricula1), R.drawable.a2019030344));
        list.add(new alumnoItem(getString(R.string.nombre2), getString(R.string.matricula2), R.drawable.a2020030174));
        list.add(new alumnoItem(getString(R.string.nombre3), getString(R.string.matricula3), R.drawable.a2020030176));
        list.add(new alumnoItem(getString(R.string.nombre4), getString(R.string.matricula4), R.drawable.a2020030181));
        list.add(new alumnoItem(getString(R.string.nombre5), getString(R.string.matricula5), R.drawable.a2020030184));
        list.add(new alumnoItem(getString(R.string.nombre6), getString(R.string.matricula6), R.drawable.a2020030189));
        list.add(new alumnoItem(getString(R.string.nombre7), getString(R.string.matricula7), R.drawable.a2020030199));
        list.add(new alumnoItem(getString(R.string.nombre8), getString(R.string.matricula8), R.drawable.a2020030212));
        list.add(new alumnoItem(getString(R.string.nombre9), getString(R.string.matricula9), R.drawable.a2020030241));
        list.add(new alumnoItem(getString(R.string.nombre10), getString(R.string.matricula10), R.drawable.a2020030243));
        list.add(new alumnoItem(getString(R.string.nombre11), getString(R.string.matricula11), R.drawable.a2020030249));
        list.add(new alumnoItem(getString(R.string.nombre12), getString(R.string.matricula12), R.drawable.a2020030264));
        list.add(new alumnoItem(getString(R.string.nombre13), getString(R.string.matricula13), R.drawable.a2020030268));
        list.add(new alumnoItem(getString(R.string.nombre14), getString(R.string.matricula14), R.drawable.a2020030292));
        list.add(new alumnoItem(getString(R.string.nombre15), getString(R.string.matricula15), R.drawable.a2020030304));
        list.add(new alumnoItem(getString(R.string.nombre16), getString(R.string.matricula16), R.drawable.a2020030306));
        list.add(new alumnoItem(getString(R.string.nombre17), getString(R.string.matricula17), R.drawable.a2020030313));
        list.add(new alumnoItem(getString(R.string.nombre18), getString(R.string.matricula18), R.drawable.a2020030315));
        list.add(new alumnoItem(getString(R.string.nombre19), getString(R.string.matricula19), R.drawable.a2020030322));
        list.add(new alumnoItem(getString(R.string.nombre20), getString(R.string.matricula20), R.drawable.a2020030325));
        list.add(new alumnoItem(getString(R.string.nombre21), getString(R.string.matricula21), R.drawable.a2020030327));
        list.add(new alumnoItem(getString(R.string.nombre22), getString(R.string.matricula22), R.drawable.a2020030329));
        list.add(new alumnoItem(getString(R.string.nombre23), getString(R.string.matricula23), R.drawable.a2020030332));
        list.add(new alumnoItem(getString(R.string.nombre24), getString(R.string.matricula24), R.drawable.a2020030333));
        list.add(new alumnoItem(getString(R.string.nombre25), getString(R.string.matricula25), R.drawable.a2020030389));
        list.add(new alumnoItem(getString(R.string.nombre26), getString(R.string.matricula26), R.drawable.a2020030766));
        list.add(new alumnoItem(getString(R.string.nombre27), getString(R.string.matricula27), R.drawable.a2020030771));
        list.add(new alumnoItem(getString(R.string.nombre28), getString(R.string.matricula28), R.drawable.a2020030777));
        list.add(new alumnoItem(getString(R.string.nombre29), getString(R.string.matricula29), R.drawable.a2020030799));
        list.add(new alumnoItem(getString(R.string.nombre30), getString(R.string.matricula30), R.drawable.a2020030808));
        list.add(new alumnoItem(getString(R.string.nombre31), getString(R.string.matricula31), R.drawable.a2020030819));
        list.add(new alumnoItem(getString(R.string.nombre32), getString(R.string.matricula32), R.drawable.a2020030865));

        lv = (ListView) findViewById(R.id.listView1);


        adapter = new AdapterListView(this, R.layout.alumno, R.id.lblNombre, list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                Toast.makeText(adapterView.getContext(), getString(R.string.AlumnoSeleccionado).toString() + " " + ((alumnoItem) adapterView.getItemAtPosition(i)).getTxtNombre(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

}
