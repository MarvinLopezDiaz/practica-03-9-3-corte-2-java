package com.example.practica039_3corte2java;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterListView extends ArrayAdapter<alumnoItem> implements Filterable{

    private int groupId;
    private Activity context;
    private List<alumnoItem> originalList;
    private List<alumnoItem> filteredList;
    private LayoutInflater inflater;
    private ItemFilter itemFilter;

    public AdapterListView(Activity context, int groupId, int id, List<alumnoItem> list) {
        super(context, id, list);
        this.context = context;
        this.groupId = groupId;
        this.originalList = new ArrayList<>(list);
        this.filteredList = new ArrayList<>(list);
        inflater = context.getLayoutInflater();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupId, parent, false);
        ImageView imagenAlumno = itemView.findViewById(R.id.imgAlum);
        imagenAlumno.setImageResource(filteredList.get(position).getImageId());
        TextView textNombre = itemView.findViewById(R.id.lblNombre);
        textNombre.setText(filteredList.get(position).getTxtNombre());
        TextView textMatricula = itemView.findViewById(R.id.lblMatricula);
        textMatricula.setText(filteredList.get(position).getTxtMatricula());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    @Override
    public alumnoItem getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public Filter getFilter() {
        if (itemFilter == null) {
            itemFilter = new ItemFilter();
        }
        return itemFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            String filterString = constraint.toString().toLowerCase();

            if (filterString.isEmpty()) {
                results.count = originalList.size();
                results.values = originalList;
            } else {
                ArrayList<alumnoItem> filteredItems = new ArrayList<>();

                for (alumnoItem item : originalList) {
                    if (item.getTxtNombre().toLowerCase().contains(filterString)) {
                        filteredItems.add(item);
                    }
                }

                results.count = filteredItems.size();
                results.values = filteredItems;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (List<alumnoItem>) results.values;
            notifyDataSetChanged();
        }
    }

}
