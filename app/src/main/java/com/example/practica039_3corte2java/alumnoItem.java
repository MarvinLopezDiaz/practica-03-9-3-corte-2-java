package com.example.practica039_3corte2java;

public class alumnoItem {
    private String txtNombre;
    private String txtMatricula;
    private Integer imageId;

    public alumnoItem(String text, String text2, Integer imageId){
        this.txtNombre = text;
        this.txtMatricula = text2;
        this.imageId = imageId;
    }

    public String getTxtNombre() { return txtNombre; }

    public void getTxtNombre(String text2) { this.txtNombre = text2; }

    public String getTxtMatricula(){ return txtMatricula; }

    public void setTxtMatricula(String text){
        this.txtMatricula = text;
    }

    public Integer getImageId(){
        return imageId;
    }

    public void setImageId(Integer imageId){
        this.imageId = imageId;
    }
}
